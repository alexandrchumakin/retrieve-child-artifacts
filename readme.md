# Retrieve GitLab child artifacts
The purpose of this project is to show how to retrieve artifacts from downstream pipeline to parent pipeline. 
It might be used for external projects that need to use artifacts from remote project where it's not possible to retrieve it from downstream pipelines.

More information could be found [here](https://medium.com/@achumakin/retrieve-gitlab-ci-artifacts-from-child-pipelines-f427d1983d87).