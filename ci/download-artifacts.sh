#!/bin/bash

perform_gitlab_call() {
  path=$1
  result=$(curl --silent --insecure --noproxy '*' --header "PRIVATE-TOKEN:$ACCESS_TOKEN" "$CI_API_V4_URL/projects/$CI_PROJECT_ID/$path")
  echo "$result"
}

download_artifacts() {
  build_job_id=$1
  if [[ "$build_job_id" == "null" ]]; then
    echo "build job id is null"
  else
    echo "Downloading artifacts from job $build_job_id"
    curl --silent --insecure --noproxy '*' -L -o artifacts.zip --header "PRIVATE-TOKEN:$ACCESS_TOKEN" "$CI_API_V4_URL/projects/$CI_PROJECT_ID/jobs/$build_job_id/artifacts"
    unzip -o -qq artifacts.zip

    echo "Check if artifacts are extracted successfully"
    ls *.txt
    rm -rf artifacts.zip
  fi
}

find_publish_job_id() {
  job_name=$1
  result=$(perform_gitlab_call "pipelines/$ci_child_pipeline_id/jobs" |
    jq --raw-output '[.[]|select(.name=='\"$job_name\"')][0].id')
  echo $result
}

get_pipelines_data() {
  pipeline_id=$1
  echo "Trying to get artifacts from pipeline $pipeline_id"
  ci_child_pipeline_id=$(perform_gitlab_call "pipelines/$pipeline_id/bridges" |
    jq '[.[]|select(.name=="trigger-child-pipeline")][0].downstream_pipeline.id')
  echo "Found child pipeline id $ci_child_pipeline_id"

  project1_build_job_id=$(find_publish_job_id publish-project_1)
  project2_build_job_id=$(find_publish_job_id publish-project_2)
  printf "Project1 child pipeline id: %s\nProject1 child pipeline id: %s\n" "$project1_build_job_id" "$project2_build_job_id"

  if [[ "$project1_build_job_id" == "null" ]] && [[ "$project2_build_job_id" == "null" ]]; then
    echo "Cannot find any artifacts for project1 or project2 pipelines, try to download it from another pipeline"
    echo "Found ${#pipelines_list[@]} pipelines for branch $CI_COMMIT_REF_NAME to scan"
    echo "${pipelines_list[@]}"
    add_id=${pipelines_list[0]}
    if [ -z "$add_id" ]; then return ; else echo "Retrieved additional pipeline id $add_id"; fi
    unset pipelines_list[0]
    pipelines_list=( ${pipelines_list[@]} )
    get_pipelines_data $add_id
  else
    download_artifacts "$project1_build_job_id"
    download_artifacts "$project2_build_job_id"
  fi
}

pipelines_list=($(perform_gitlab_call "/pipelines?ref=$CI_COMMIT_REF_NAME&status=success" | jq --raw-output '.[].id | @sh'))
get_pipelines_data $CI_PIPELINE_ID
