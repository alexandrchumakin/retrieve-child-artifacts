import os
from pathlib import Path

modules = os.environ['MODULES']
print(modules)
ci_template = Path(modules and '.child-template.yml' or '.no-modules.yml').read_text()
modules_list = modules and modules.split('\n') or []

for module in modules_list:
    ci_template += f"""
publish-{module}:
  variables:
    MODULE_NAME: {module}
  <<: *publish_artifact
"""

Path('../.publish-artifacts.yml').write_text(ci_template)
