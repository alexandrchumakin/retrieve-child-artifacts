import random

modules = ['project_1', 'project_2']

for module in modules:
    print(module) if bool(random.getrandbits(1)) else None
